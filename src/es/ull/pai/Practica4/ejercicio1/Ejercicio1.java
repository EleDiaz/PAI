package es.ull.pai.Practica4.ejercicio1;

import javax.swing.*;
import java.awt.*;

/**
 * This program wait a input by user to predict a number hidden by program, if user
 * is near or equal to random number is gains several awards
 */
public class Ejercicio1 extends JFrame {

    Ejercicio1() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 20));
        add(new JLabel("hola"));
    }

    public static void main(String[] args) {
        Ejercicio1 frame = new Ejercicio1();

        frame.setTitle("Ejercicio 1");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
