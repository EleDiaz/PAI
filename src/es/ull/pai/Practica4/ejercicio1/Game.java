package es.ull.pai.Practica4.ejercicio1;

import java.util.Random;

/**
 *
 * @author Eleazar Díaz Delgado
 */
public class Game {
    private Integer secretNum;

    public Game() {
        Random rand = new Random();
        secretNum = rand.nextInt(1000);
    }

    /**
     * User guess the number, its receive greater prize
     * @return Boolean;
     */
    public Boolean isNumber(Integer number) {
        return number.equals(secretNum);
    }

    /**
     * let x num = if num == 0 then [] else num : x (num `div` 10)
     * let coincident num = (>=3) $ length $ filter (==) $ zipWith (x 100) (repeat secret) <*> zipWith (x 100) (repeat num)
     * @param number
     * @return
     */
    public Boolean isCoincident(Integer number) {
        final Integer INT_SIZE = 3;
        Boolean isCoincident = true;

        int i = 1;
        while (isCoincident && i <= INT_SIZE) {
            Integer secretDigit = secretNum % (int) (Math.pow(10, i));
            Boolean found = false;
            int j = 1;
            while (!found && j <= INT_SIZE) {
                found = secretDigit == number % Math.pow(10,j);
                j++;
            }

            isCoincident = isCoincident && found;
            i++;
        }
    return isCoincident;
    }
}
