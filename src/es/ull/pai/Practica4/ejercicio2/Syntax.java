package es.ull.pai.Practica4.ejercicio2;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Analyzer syntax from a raw text, contains java code, and generate a HTML file with highlight keywords, comments....
 * Created by eleazardd on 6/03/16.
 */
public class Syntax {
    private StringBuilder formated;
    private int posRead;
    private ArrayList<Annotation> annotations;

    public Syntax(String text) {
        formated = new StringBuilder();
        annotations = new ArrayList<>();
        header();
        keywordsFinder(text);
        commentFinder(text);
        stringLiteralsFinder(text);
        newlineFinder(text);
        annotations.sort((Annotation a, Annotation b) -> a.compareTo(b));

        int lastElementEnd = -1;
        int i = 0;
        while (i < text.length()) {
            if (annotations.size()>=1) {
                lastElementEnd = annotations.get(0).getEnd();
                while (i < annotations.get(0).getStart()) {
                    Character ch = text.charAt(i);
                    formated.append(ch);
                    i++;
                }
                formated.append(annotations.get(0).getStartAnnon());
                while (i < annotations.get(0).getEnd()) {
                    Character ch = text.charAt(i);
                    formated.append(ch);
                    i++;
                }
                formated.append(annotations.get(0).getEndAnnon());
                annotations.remove(0);

                while (annotations.size() > 0 && lastElementEnd > annotations.get(0).getStart()) {
                    annotations.remove(0);
                }
            }
            else {
                Character ch = text.charAt(i);
                formated.append(ch);
                i++;
            }
        }
        footer();
    }

    public String toString() {
        return formated.toString();
    }

    private void header() {
        formated.append("<pre>");
    }

    private void footer() {
        formated.append("</pre>");
    }

    private void newlineFinder(String text) {
        Pattern p = Pattern.compile("(\\n)");

        Matcher match = p.matcher(text);
        while (match.find()) {
            String elem = match.group();
            System.out.println(elem);

            System.out.println(match.start());

            annotations.add(new Annotation(match.start(), match.end(), "<br>", "</br>"));
        }
    }

    private void keywordsFinder(String text) {
        Pattern p = Pattern.compile("\\s(abstract|continue|for|new|switch|assert|default|goto"
                + "|package|synchronized|boolean|do|if|private|this|break"
                + "|double|implements|protected|throw|byte|else|import|public"
                + "|throws|case|enum|instanceof|return|transient|catch|extends"
                + "|int|short|try|char|final|interface|static|void|class|finally"
                + "|long|strictfp|volatile|const|float|native|super|while)\\s");

        Matcher match = p.matcher(text);
        while (match.find()) {
            String elem = match.group();
            System.out.println(elem);

            System.out.println(match.start());

            annotations.add(new Annotation(match.start(), match.end(), "<span style=\"color:black\"><b>", "</b></span>"));
        }
    }

    private void commentFinder(String text) {
        Pattern p = Pattern.compile("(//.*\\n|/\\*(\\n|.)*?\\*/)");

        Matcher match = p.matcher(text);
        while (match.find()) {
            String elem = match.group();
            System.out.println(elem);

            System.out.println(match.start());

            annotations.add(new Annotation(match.start(), match.end(), "<span style=\"color:green\">", "</span>"));
        }
    }

    private void stringLiteralsFinder(String text) {
        Pattern p = Pattern.compile("\"(\\Q\\\"\\E|.)*?\"");

        Matcher match = p.matcher(text);
        while (match.find()) {
            String elem = match.group();
            System.out.println(elem);

            System.out.println(match.start());
            annotations.add(new Annotation(match.start(), match.end(), "<span style=\"color:blue\">", "</span>"));
        }
    }
}
