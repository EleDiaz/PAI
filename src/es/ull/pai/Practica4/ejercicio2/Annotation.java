package es.ull.pai.Practica4.ejercicio2;



/**
 * Created by eleazardd on 7/03/16.
 */
public class Annotation implements Comparable<Annotation> {
    private Integer start;
    private Integer end;
    private String startAnnon;
    private String endAnnon;

    public Annotation(int ini, int fin, String sAnnon, String eAnnon) {
        start = ini;
        end   = fin;
        startAnnon = sAnnon;
        endAnnon   = eAnnon;
    }

    public int compareTo(Annotation b) {
        return start.compareTo(b.start);
    }

    public Integer getStart() {
        return start;
    }

    public Integer getEnd() {
        return end;
    }

    public String getEndAnnon() {
        return endAnnon;
    }

    public String getStartAnnon() {
        return startAnnon;
    }

}
