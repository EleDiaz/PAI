package es.ull.pai.Practica4.ejercicio5;

/**
 * Created by eleazardd on 7/03/16.
 */
public class HexFormatException extends Exception {

    public HexFormatException(String msg) {
        super(msg);
    }
}

