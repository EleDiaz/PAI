package es.ull.pai.Practica4.ejercicio3;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by eleazardd on 6/03/16.
 */
public class Ejercicio3 extends JFrame {
    Ejercicio3() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 20));
        add(new JLabel("hola"));
    }

    public static void main(String[] args) {
        Ejercicio3 frame = new Ejercicio3();
        final String USAGE = new String("USAGE: ./practica3 file  # where must be a file");
        es.ull.pai.Practica4.ejercicio2.ParseCommands commands = new es.ull.pai.Practica4.ejercicio2.ParseCommands(args, USAGE);
        try {
            String content = new String(Files.readAllBytes(Paths.get(commands.getString())));

            for (String word : deleteDuplicates(content)) {
                System.out.println(word);
            }
        }
        catch (FileNotFoundException err) {
            System.out.println("No se encontro el archivo");
        }
        catch (IOException err) {
            System.out.println("Error al cargar el archivo");
        }

        frame.setTitle("Ejercicio 3");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static ArrayList<String> deleteDuplicates(String text) {
        String[] words = text.split(" \n");
        TreeMap<String, Integer> counts = new TreeMap<>();
        for (String word : words) {
            if (counts.containsKey(word)) {
                counts.put(word, counts.get(word)+1);
            }
            else {
                counts.put(word, 0);
            }
        }
        ArrayList<String> result = new ArrayList<>();
        for (String word : counts.keySet()) {
            result.add(word);
        }
        return result;
    }
}